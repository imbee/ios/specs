Pod::Spec.new do |s|
    s.name              = 'IMbeeUI'
    s.version           = '0.3.1.46'
    s.summary           = 'IMbee UI SDK'
    s.homepage          = 'https://docs.imbee.es/ios/imbee-ui/'

    s.author            = { 'IMbee' => 'info@imbeemessenger.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => "https://gitlab.com/imbee/ios/specs/raw/master/IMbeeUI/0.3.1.46/IMbeeUI-0.3.1.46.zip" }

    s.pod_target_xcconfig = {
        'ENABLE_BITCODE' => 'YES',
        'SWIFT_VERSION' => '4.2'
    }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'IMbeeUI.framework'
    s.ios.dependency 'IMbeeCore', '0.9.9g'
    s.ios.dependency 'Haneke', '~> 1.0'
    s.ios.dependency 'KAProgressLabel', '~> 3.3'
end

