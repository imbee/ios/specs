Pod::Spec.new do |s|
    s.name              = 'IMbeeUI'
    s.version           = '0.3.1.48'
    s.summary           = 'IMbee UI SDK'
    s.homepage          = 'https://docs.imbee.es/ios/imbee-ui/'

    s.author            = { 'IMbee' => 'info@imbeemessenger.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => "https://gitlab.com/imbee/ios/specs/raw/master/IMbeeUI/0.3.1.48/IMbeeUI-0.3.1.48.zip" }

    s.pod_target_xcconfig = {
        'ENABLE_BITCODE' => 'YES',
        'SWIFT_VERSION' => '4.2'
    }

    s.ios.deployment_target = '14.1'
    s.ios.vendored_frameworks = 'IMbeeUI.framework'
    s.ios.dependency 'IMbeeCore', '0.9.9g'
    s.ios.dependency 'XMPPFramework', '3.7.666'
    s.ios.dependency 'KissXML', '5.2.666'
    s.ios.dependency 'libidn', '1.33.666'
    s.ios.dependency 'sqlite3', '3.45.666'
    s.ios.dependency 'TTTAttributedLabel', '2.0.666'
    s.ios.dependency 'KAProgressLabel', '3.3.666'
    s.ios.dependency 'Haneke', '1.0.666'
end

